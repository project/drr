(function($, Drupal) {
    Drupal.behaviors.ddr = {
    attach:function(context, settings){
      var ua = navigator.userAgent,
      open = false,
      pickclick = (ua.match(/iPad/i) || ua.match(/iPhone/)) ? "touchstart" : "click";

      ddr_init();

      function ddr_init() {
        $('body').append('<div class="backdrop-drawer-modal"></div>');
        $('body').append('<div class="wrapper-drawer-modal"></div>');
        var mediaQuerie = window.matchMedia("(max-width: 768px)");
        ddr_mobile(mediaQuerie);
        mediaQuerie.addListener(ddr_mobile);
      }

      function ddr_mobile(mediaQuerie) {
        if (mediaQuerie.matches) {
            mediaQuerieModal =  true;
        } else {
            mediaQuerieModal = false;
        }
      }

      function ddr_get_content(link){
        var $request;
        if ($request != null){
            $request.abort();
            $request = null;
        }

          $request = $.ajax({
          url: link,
          async: false,
          success: function(data) {
            $('.drawer-modal-content').append(data);
            $('.loader-drawer-modal').hide();
            loaded();
          }
        });
      }

      function ddr_open_modal(link) {
        $('.wrapper-drawer-modal').append(
          '<div class="loader-drawer-modal"><img src="/sites/all/modules/custom/krot_apachesolr_infinite_scroll/images/ajax-loader.gif" alt="loading..."></div>' +
          '<div class="drawer-modal-close-icon">'+
          '<a href="#" class="drawer-modal-close">' +
          '<i class="fa fa-times-circle"></i></div>'+
          '</a>' +
          '<div class="drawer-modal-content"></div>' +
          '</div>');

          ddr_get_content(link);
          open = true;

      }

      $('.drawer-modal').on('click', function() {
        if (open === false) {
          $('.loader-drawer-modal').remove();
          $('.wrapper-drawer-modal').append('<div class="loader-drawer-modal"><img src="/sites/all/modules/custom/krot_apachesolr_infinite_scroll/images/ajax-loader.gif" alt="loading..."></div>');

          var scope = $(this);
          config_duration = ((scope.attr('data-drawer-modal-duration')) ? scope.data('drawer-modal-duration') : settings.ddr.drawer_configuration_duration);
          var width_desktop = ((scope.attr('data-drawer-modal-width-desktop')) ? scope.data('drawer-modal-width-desktop') : settings.ddr.drawer_configuration_width_desktop);
          var width_mobile = ((scope.attr('data-drawer-modal-width-mobile')) ? scope.data('drawer-modal-width-mobile') : settings.ddr.drawer_configuration_width_mobile);
          var is_mobile = mediaQuerieModal;

          if(is_mobile == true) {
            open_drawer_width = width_mobile + '%';
          }
          else {
            open_drawer_width = width_desktop + '%';
          }

          $('body').css('overflow', 'hidden');
          $('.wrapper-drawer-modal', context).once().show().animate(
                                              {width: open_drawer_width},
                                              {duration: config_duration * 1000,
                                              complete: function(){
                                                ddr_open_modal(scope.attr('href'));
                                              }
                                              });
          $('.backdrop-drawer-modal').fadeIn(config_duration * 1000);
        }
        return false;
      });

      $(document).on(pickclick, '.drawer-modal-close, .backdrop-drawer-modal', function() {
        if (open === true) {
          $('.drawer-modal-close').hide();
          $('body').css('overflow', 'inherit');
          $('.drawer-modal-content').remove();
          $('.backdrop-drawer-modal').fadeOut(config_duration * 1000);

          $('.wrapper-drawer-modal').animate(
            {width: "0"},
            {duration: config_duration * 1000,
              complete: function(){
                $('.wrapper-drawer-modal').remove();
                $('body').append('<div class="wrapper-drawer-modal"></div>');
              }
            });
            open = false;
          }
        return false;
      });

      var myScroll;
      function loaded () {
        myScroll = new IScroll('.drawer-modal-content', {
          scrollbars: true,
          mouseWheel: true,
          interactiveScrollbars: true,
          shrinkScrollbars: 'scale',
          fadeScrollbars: true
        });
      }

    }
  };
}(jQuery, Drupal));