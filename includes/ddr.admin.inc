<?php

function ddr_configuration_form() {
  $form = array();

  $form['drawer_configuration_duration'] = array(
    '#type' => 'textfield',
    '#attributes' => array(
        ' type' =>'number',
        ' min' => '0',
        ' max' => '10',
        ' step' => '0.5',
    ),
    '#title' => t('Duration'),
    '#maxlength' => 5,
    '#default_value' => variable_get('drawer_configuration_duration','0'),
    '#description' => t('Type how many seconds you want the animation to last'),
    '#weight' => '2'
  );

  $form['drawer_configuration_width_mobile'] = array(
    '#type' => 'textfield',
    '#attributes' => array(
        ' type' => 'number',
        ' min' => '55',
        ' max' => '100',
    ),
    '#title' => t('Drawer Size - Mobile (max 768 width)'),
    '#maxlength' => 3,
    '#field_suffix' => '%',
    '#default_value' => variable_get('drawer_configuration_width_mobile','90'),
    '#description' => t('Enter the width of your Drawer'),
    '#weight' => '3'
  );

  $form['drawer_configuration_width_desktop'] = array(
    '#type' => 'textfield',
    '#attributes' => array(
        ' type' => 'number',
        ' min' => '55',
        ' max' => '100',
    ),
    '#title' => t('Drawer Size - Desktop'),
    '#maxlength' => 3,
    '#field_suffix' => '%',
    '#default_value' => variable_get('drawer_configuration_width_desktop','55'),
    '#description' => t('Enter the width of your Drawer'),
    '#weight' => '4'
  );

  return system_settings_form($form);
}